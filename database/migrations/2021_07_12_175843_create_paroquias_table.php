<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParoquiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paroquias', function (Blueprint $table) {
            $table->id();
            $table->string("nome");
            $table->string("paroco");
            $table->integer("usuario_responsavel");
            $table->string("logomarca",255)->nullable();
            $table->string("endereco",255);
            $table->char("telefone_fixo",13)->nullable();
            $table->char("telefone_celular",14)->nullable();
            $table->char("whatsapp",14)->nullable();
            $table->char("telegram",14)->nullable();
            $table->char("signal",14)->nullable();
            $table->string("email",100)->nullable();
            $table->string("site",100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paroquias');
    }
}
