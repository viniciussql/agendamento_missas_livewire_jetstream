<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantesHasMissasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participantes_has_missas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("missas_id");
            $table->foreign("missas_id")->references("id")->on("missas");
            $table->unsignedBigInteger("participantes_id");
            $table->foreign("participantes_id")->references("id")->on("participantes_missa");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participantes_has_missas');
    }
}
