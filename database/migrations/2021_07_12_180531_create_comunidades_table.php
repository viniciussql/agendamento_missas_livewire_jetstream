<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComunidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunidades', function (Blueprint $table) {
            $table->id();
            $table->string("nome");            
            $table->unsignedBigInteger("paroquia_id");
            $table->foreign("paroquia_id")->references("id")->on("paroquias");
            $table->integer("usuario_responsavel");
            $table->string("coordenador_1");
            $table->char("telefone_coordenador_1",14)->nullable();
            $table->string("coordenador_2");
            $table->char("telefone_coordenador_2",14)->nullable();
            $table->string("endereco",255);
            $table->string("bairro",255);
            $table->string("cidade",155);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunidades');
    }
}
