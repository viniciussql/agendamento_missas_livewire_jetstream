<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missas', function (Blueprint $table) {
            $table->id();
            $table->date("data_missa");
            $table->time("hora_missa");
            $table->integer("numero_vagas");
            $table->integer("vagas_preenchidas")->nullable();
            $table->string("celebrante")->nullable();
            $table->unsignedBigInteger("comunidades_id");
            $table->foreign("comunidades_id")->references("id")->on("comunidades");
            $table->unsignedBigInteger("users_id");
            $table->foreign("users_id")->references("id")->on("users");
            $table->boolean("ativo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missas');
    }
}
