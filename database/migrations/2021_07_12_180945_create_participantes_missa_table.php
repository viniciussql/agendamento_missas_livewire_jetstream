<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantesMissaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participantes_missa', function (Blueprint $table) {
            $table->id();
            $table->string("nome"); 
            $table->string("logradouro")->nullable(); 
            $table->integer("numero")->nullable();
            $table->string("complemento",100)->nullable();
            $table->char("telefone_principal",14);
            $table->unsignedBigInteger("users_id");
            $table->foreign("users_id")->references("id")->on("users");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participantes_missa');
    }
}
