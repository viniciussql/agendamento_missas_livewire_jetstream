<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Paroquia;
use App\Models\Comunidade;
use App\Models\Log;
use Livewire\WithFileUploads;
use App\Http\Livewire\Session;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use DB;

class ComunidadeControlador extends Component
{
    use WithPagination;
    use WithFileUploads;

    /**VARIÁVEIS E PROPRIEDADES PÚBLICAS */
    public $busca;
    public $informacoesParoquia;
    public $informacoesComunidade;
    public $comunidade_id = 0;
    public $nome;
    public $paroquia_id = "-1";
    public $usuario_responsavel;
    public $coordenador_1;
    public $telefone_coordenador_1;
    public $coordenador_2;
    public $telefone_coordenador_2;
    public $endereco;
    public $bairro;
    public $cidade;
    public $paroquias;
    public $modalMaisInformacoes = false;
    public $modalConfirmarExclusao = false;
    private $pagination = 20;
    public $endereco_paroquia;

    public function render()
    {
        //COMBO PARÓQUIAS
        $this->paroquias = Paroquia::orderBy("nome", "asc")->get();
        
        if (strlen($this->busca) > 0) {
            $info = Comunidade::leftjoin("paroquias as p", "p.id", "comunidades.paroquia_id")
                ->where("comunidades.nome", "like", "%" . $this->busca . "%")
                ->orWhere("comunidades.endereco", "like", "%" . $this->busca . "%")
                ->orWhere("comunidades.bairro", "like", "%" . $this->busca . "%")
                ->orWhere("comunidades.cidade", "like", "%" . $this->busca . "%")
                ->orWhere("p.nome", "like", "%" . $this->busca . "%")
                ->orWhere("p.paroco", "like", "%" . $this->busca . "%")
                ->select("comunidades.*", "p.nome as paroquia")
                ->orderBy("comunidades.nome", "asc")
                ->paginate($this->pagination);
            return view(
                'livewire.comunidade',
                [
                    "info" => $info,
                    "paroquias" => $this->paroquias,
                ]
            );
        } else {
            $info = Comunidade::leftjoin("paroquias as p", "p.id", "comunidades.paroquia_id")
                ->select("comunidades.*", "p.nome as paroquia")
                ->orderBy("comunidades.nome", "asc")
                ->paginate($this->pagination);
            return view(
                'livewire.comunidade',
                [
                    "info" => $info,
                    "paroquias" => $this->paroquias,
                ]
            );
        }
    }

    /**BUSCAS COM PAGINAÇÃO */
    public function updatingBusca(): void
    {
        $this->gotoPage(1);
    }

    public function criar()
    {
        $this->limparCampos();
    }

    public function abrirModalMaisInformacoes()
    {
        $this->modalMaisInformacoes = true;
    }
    public function fecharModalMaisInformacoes()
    {
        $this->modalMaisInformacoes = false;
    }
    public function abrirModalConfirmarExclusao($id)
    {
        $this->modalConfirmarExclusao = $id;
    }
    public function fecharModalConfirmarExclusao()
    {
        $this->modalConfirmarExclusao = false;
    }

    public function limparCampos()
    {
        $this->busca = "";
        $this->informacoesParoquia = "";
        $this->informacoesComunidade = "";
        $this->comunidade_id = 0;
        $this->paroquia_id = "-1";
        $this->nome = "";
        $this->coordenador_1 = "";
        $this->coordenador_2 = "";
        $this->telefone_coordenador_1 = "";
        $this->telefone_coordenador_2 = "";
        $this->endereco = "";
        $this->bairro = "";
        $this->cidade = "";
        $this->endereco_paroquia = "";
    }

    public function editar($id)
    {
        $comunidade = Comunidade::findOrFail($id);
        $this->comunidade_id = $id;
        $this->nome = $comunidade->nome;
        $this->paroquia_id = $comunidade->paroquia_id;
        $this->coordenador_1 = $comunidade->coordenador_1;
        $this->telefone_coordenador_1 = $comunidade->telefone_coordenador_1;
        $this->coordenador_2 = $comunidade->coordenador_2;
        $this->telefone_coordenador_2 = $comunidade->telefone_coordenador_2;
        $this->endereco = $comunidade->endereco;
        $this->bairro = $comunidade->bairro;
        $this->cidade = $comunidade->cidade;
        $this->endereco_paroquia = $comunidade->endereco;
    }

    public function carregarMaisInformacoes($id)
    {
        $this->informacoesComunidade = Comunidade::findOrFail($id);
        $this->abrirModalMaisInformacoes();
    }

    public function excluir($id)
    {
        $objComunidade = Comunidade::find($id);
        $nomeComunidade = $objComunidade->nome;
        $comunidade = Comunidade::find($id)->delete();
        $log = Log::create([
            "acao" => "Comunidade excluída: " . $nomeComunidade,
            "autor_id" => Auth::user()->id,
        ]);
        session()->flash("message", "COMUNIDADE excluída com sucesso");

        $this->fecharModalConfirmarExclusao();
    }

    public function salvar()
    {
        $regras = [
            'nome' => 'required',
            'paroquia_id' => 'not_in:-1',
            'coordenador_1' => 'required',
            'coordenador_2' => 'required',
            'endereco' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
        ];

        $mensagens = [
            'nome.required' => 'Informe o NOME da comunidade!',
            'paroquia_id.not_in' => 'Informe a qual PARÓQUIA pertence a comunidade!',
            'coordenador_1.required' => 'Informe o nome do COORDENADOR da comunidade',
            'coordenador_2.required' => 'Informe o nome do outro COORDENADOR da comunidade',
            'endereco.required' => 'Informe o ENDEREÇO da comunidade!',
            'bairro.required' => 'Informe o BAIRRO da comunidade!',
            'cidade.required' => 'Informe a CIDADE da comunidade!',
        ];

        $this->validate($regras, $mensagens);

        /**NOVO REGISTRO - CRIANDO */
        if ($this->comunidade_id <= 0) {
            $comunidade = Comunidade::create([
                "nome" => $this->nome,
                "paroquia_id" => $this->paroquia_id,
                "usuario_responsavel" => Auth::user()->id,
                "coordenador_1" => $this->coordenador_1,
                "telefone_coordenador_1" => $this->telefone_coordenador_1,
                "coordenador_2" => $this->coordenador_2,
                "telefone_coordenador_2" => $this->telefone_coordenador_2,
                "endereco" => $this->endereco,
                "bairro" => $this->bairro,
                "cidade" => $this->cidade,
            ]);
        }
        /**REGISTRO EXISTENTE - EDITANDO */
        else {
            $comunidade = Comunidade::find($this->comunidade_id);
            $comunidade->update([
                "nome" => $this->nome,
                "paroquia_id" => $this->paroquia_id,
                "usuario_responsavel" => Auth::user()->id,
                "coordenador_1" => $this->coordenador_1,
                "telefone_coordenador_1" => $this->telefone_coordenador_1,
                "coordenador_2" => $this->coordenador_2,
                "telefone_coordenador_2" => $this->telefone_coordenador_2,
                "endereco" => $this->endereco,
                "bairro" => $this->bairro,
                "cidade" => $this->cidade,
            ]);
        }

        $log = Log::create([
            "acao" => "Comunidade cadastrada ou editada: " . $this->nome,
            "autor_id" => Auth::user()->id,
        ]);

        session()->flash(
            "message",
            $this->comunidade_id ? "Comunidade ATUALIZADA com sucesso: " . $this->nome : "Comunidade INCLUÍDA com sucesso: " . $this->nome . ""
        );
        $this->limparCampos();
    }

    /**OUVINTES - ESCUTAR EVENTOS E EXECUTAR AÇÕES */
    protected $listeners = [
        "exibirEnderecoParoquia" => "exibirEnderecoParoquia",
    ];

    public function exibirEnderecoParoquia($id)
    {
        $registro = Paroquia::find($id);        
        if ($registro) {
           $this->endereco_paroquia = $registro->endereco;            
        } else {
            $this->endereco_paroquia = "";
        }
    }
}
