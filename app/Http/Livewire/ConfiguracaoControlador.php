<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Configuracao;
use App\Models\Log;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DB;

class ConfiguracaoControlador extends Component
{
    /**DEFINIÇÕES DE VARIÁVEIS */
    public $configuracao_id;
    public $email_sistema_notificacoes;
    public $email_secretario_notificacoes;
    public $mensagem_biblica_rodape;

    public function mount(){
        $configuracao = Configuracao::all();
        if ($configuracao->count() > 0) {
            $this->email_sistema_notificacoes = $configuracao[0]->email_sistema_notificacoes;
            $this->email_secretario_notificacoes = $configuracao[0]->email_secretario_notificacoes;
            $this->mensagem_biblica_rodape = $configuracao[0]->mensagem_biblica_rodape;
        }
    }

    public function render()
    {
        return view('livewire.configuracao');
    }

    public function salvar(){
        $regras = [
            'email_sistema_notificacoes' => 'required|email',
            'email_secretario_notificacoes' => 'required|email',
            'mensagem_biblica_rodape' => 'required',
        ];

        $mensagens = [
            'email_sistema_notificacoes.required' => 'Informe um e-mail para notificações do sistema',
            'email_sistema_notificacoes.email' => 'Informe um e-mail válido',
            'email_secretario_notificacoes.required' => 'Informe um e-mail para notificações do(a) secretário(a) paroquial',
            'email_secretario_notificacoes.email' => 'Informe um e-mail válido',
            'mensagem_biblica_rodape.required' => 'Informe uma citação bíblica (será exibida no rodapé)',
        ];

        $this->validate($regras, $mensagens);

        DB::table('configuracoes')->truncate(); //eliminando as informações da tabela

        $configuracao = Configuracao::create([
            'email_sistema_notificacoes' => $this->email_sistema_notificacoes,
            'email_secretario_notificacoes' => $this->email_secretario_notificacoes,
            'mensagem_biblica_rodape' => $this->mensagem_biblica_rodape,            
        ]);

        $log = Log::create([
            "acao"=>"Editando configurações do sistema",
            "autor_id"=>auth()->user()->id,
        ]);

        session()->flash(
            "message",
            $this->configuracao_id ? "Registro ATUALIZADO com sucesso!" : "Registro INCLUÍDO com sucesso!" . ""
        );
    }
}
