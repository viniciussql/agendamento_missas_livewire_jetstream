<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Paroquia;
use App\Models\Comunidade;
use App\Models\Missa;
use App\Models\Log;
use App\Models\ParticipanteHasMissa;
use App\Models\ParticipanteMissa;
use DB;

class AgendamentoMissaControlador extends Component
{
    use WithPagination;

    /**VARIÁVEIS E PROPRIEDADES PÚBLICAS */
    public $agendamento_missa_id = 0;
    public $paroquiaId = "-1";
    public $comunidadesId = "-1";
    public $missaId = "-1";
    public $informacoesMissa;
    public $usuario_responsavel;
    public $comunidades;
    public $missas;
    public $objMissa;
    public $vagas_restantes_missa;
    public $nome_participante_missa;
    public $logradouro_participante_missa;
    public $numero_logradouro_participante_missa;
    public $complemento_logradouro_participante_missa;
    public $telefone_principal_participante_missa;
    public $participantesMissa = [];
    public $i = 1;
    public $modalMaisInformacoes = false;
    public $modalConfirmarExclusao = false;
    private $pagination = 20;

    public function render()
    {
        $this->paroquias = Paroquia::orderBy("nome", "asc")->get();
        $missas = Missa::leftjoin("comunidades as c", "c.id", "missas.comunidades_id")
            ->select("missas.*", "c.nome as comunidade", DB::RAW("'' as vagas_restantes"))
            ->orderBy("missas.data_missa", "desc")
            ->paginate($this->pagination);

        foreach ($missas as $m) {
            $m->vagas_restantes = $this->calcularVagasRestantes($m->id);
            $m->nome_paroquia = $this->retornarNomeParoquia($m->id);
        }

        return view(
            'livewire.agendamento-missa',
            [
                "missas" => $missas,
                "paroquias" => $this->paroquias,
                "comunidades" => $this->comunidades,
            ]
        );
    }

    public function editar($id)
    {
    }

    public function calcularVagasRestantes($idMissa)
    {
        $missa = Missa::findOrFail($idMissa);
        $numeroVagas = $missa->numero_vagas;
        $vagasPreenchidas = $missa->vagas_preenchidas;
        $vagasRestantes = $numeroVagas - $vagasPreenchidas;
        return $vagasRestantes;
    }

    public function retornarNomeParoquia($idMissa)
    {
        $missa = Missa::findOrFail($idMissa);
        $comunidade = Comunidade::findOrFail($missa->comunidades_id);
        $paroquia = Paroquia::findOrFail($comunidade->paroquia_id);
        return $paroquia->nome;
    }

    /**BUSCAS COM PAGINAÇÃO */
    public function updatingBusca(): void
    {
        $this->gotoPage(1);
    }

    public function limparCampos()
    {
        $this->agendamento_missa_id = 0;
        $this->paroquiaId = "-1";
        $this->comunidadesId = "-1";
        $this->missaId = "-1";
        $this->nome_participante_missa = "";
        $this->logradouro_participante_missa = "";
        $this->numero_logradouro_participante_missa = "";
        $this->complemento_logradouro_participante_missa = "";
        $this->telefone_principal_participante_missa = "";
        $this->vagas_restantes_missa = "";
        $this->participantesMissa = [];
    }

    public function updatedParoquiaId($idParoquia)
    {
        $this->comunidades = Comunidade::where("paroquia_id", $idParoquia)
            ->orderBy("nome", "ASC")->get();
        $this->vagas_restantes_missa = "";
    }

    public function updatedComunidadesId($idComunidade)
    {
        $this->missas = Missa::where("comunidades_id", $idComunidade)
            ->where("numero_vagas", ">", "vagas_preenchidas")
            ->whereDate('data_missa', '>=', date('Y-m-d'))
            ->orderBy("data_missa", "desc")->get();
        $this->vagas_restantes_missa = "";
    }

    public function updatedMissaId($idMissa)
    {
        $this->vagas_restantes_missa = $this->calcularVagasRestantes($idMissa);
        $this->objMissa = Missa::find($idMissa);
    }

    public function adicionarParticipante($i)
    {
        if ($this->paroquiaId <> "-1" && $this->comunidadesId <> "-1" && $this->missaId <> "-1") {
            $this->vagas_restantes_missa = $this->calcularVagasRestantes($this->missaId);
            if ($this->vagas_restantes_missa == 0){
                session()->flash(
                    "vagas",
                    "Infelizmente não há mais vagas para essa celebração :("
                );
            }
            else{
                $i = $i + 1;
                $this->i = $i;
                array_push($this->participantesMissa, $i);

                //PREENCHE VAGA
                $missa = Missa::findOrFail($this->missaId);
                $missa->preencherVaga(1);
                $this->vagas_restantes_missa = $this->calcularVagasRestantes($this->missaId);
            }
        } else {
            session()->flash(
                "vagas",
                "Informe a [PARÓQUIA], a [COMUNIDADE] e o [HORÁRIO] em que deseja participar!"
            );
        }
    }

    public function removerParticipante($i)
    {
        //dd($i);
        if ($i >= 0) {
            unset($this->participantesMissa[$i]);
            $missa = Missa::findOrFail($this->missaId);
            $missa->liberarVaga(1);
            $this->vagas_restantes_missa = $this->calcularVagasRestantes($this->missaId);
        }
    }

    public function agendarMissa()
    {
        $regras = [
            "paroquiaId" => 'not_in:-1',
            "comunidadesId" => 'not_in:-1',
            "missaId" => 'not_in:-1',
            'nome_participante_missa.*' => 'required',
            'telefone_principal_participante_missa.*' => 'required',
        ];

        $mensagens = [
            'paroquiaId.not_in' => 'Informe a PARÓQUIA onde deseja participar',
            'comunidadesId.not_in' => 'Informe em qual COMUNIDADE deseja participar',
            'missaId.not_in' => 'Informe o dia e horário em que deseja participar',
            'nome_participante_missa.*.required' => 'Informe o nome do participante',
            'telefone_principal_participante_missa.*.required' => 'Informe o telefone do participante',
        ];

        $this->validate($regras, $mensagens);

        /**SALVA DADOS DO PARTICIPANTE */       
        foreach ($this->nome_participante_missa as $key => $value) {            
            $logradouro = null;
            $numero = 0;
            $complemento = null;

            //dd($this->logradouro_participante_missa);

            if ($this->logradouro_participante_missa[$key] !=null) {               
                $logradouro = $this->logradouro_participante_missa[$key];
                //dd($logradouro);
            }
            if ($this->numero_logradouro_participante_missa[$key]) {
                $numero = intval($this->numero_logradouro_participante_missa[$key]);
            }
            if ($this->complemento_logradouro_participante_missa[$key]) {
                $complemento = $this->complemento_logradouro_participante_missa[$key];
            }

            $participantes_missa = ParticipanteMissa::create([
                "nome" => $this->nome_participante_missa[$key],
                "logradouro" => $logradouro,
                "numero" => $numero,
                "complemento" => $complemento,
                "telefone_principal" => $this->telefone_principal_participante_missa[$key],
                "users_id" => Auth::user()->id,
            ]);

            /**ASSOCIA O PARTICIPANTE À CELEBRAÇÃO */
            $participante_has_missa = ParticipanteHasMissa::create([
                "missas_id" => $this->missaId,
                "participantes_id" => $participantes_missa->id,
            ]);
        }

        $log = Log::create([
            "acao" => "Missa agendada. ID: " . $this->missaId,
            "autor_id" => Auth::user()->id,
        ]);

        session()->flash(
            "message",
            "Celebração agendada com sucesso"
        );
        $this->limparCampos();
    }
}
