<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;
use App\Models\Paroquia;
use App\Models\Log;
use Livewire\WithFileUploads;
use Livewire\Component;
use Illuminate\Support\Str;
use DB;

class ParoquiaControlador extends Component
{
    use WithPagination;
    use WithFileUploads;

    /**VARIÁVEIS E PROPRIEDADES PÚBLICAS */
    public $busca;
    public $informacoesParoquia;
    public $paroquia_id = 0;
    public $nome;
    public $paroco;
    public $usuario_responsavel;
    public $logomarca;
    public $endereco;
    public $telefone_fixo;
    public $telefone_celular;
    public $whatsapp;
    public $telegram;
    public $signal;
    public $email;
    public $site;
    public $modalMaisInformacoes = false;
    public $modalConfirmarExclusao = false;
    private $pagination = 20;

    public function render()
    {
        if (strlen($this->busca) > 0) {
            $info = Paroquia::where("nome", "like", "%" . $this->busca . "%")
                ->orWhere("paroco", "like", "%" . $this->busca . "%")
                ->orWhere("endereco", "like", "%" . $this->busca . "%")
                ->paginate($this->pagination);
            return view(
                'livewire.paroquia',
                [
                    "info" => $info,
                ]
            );
        } else {
            $info = Paroquia::select("*")->orderBy("nome")->paginate($this->pagination);
            return view(
                'livewire.paroquia',
                [
                    "info" => $info,
                ]
            );
        }
    }
    /**BUSCAS COM PAGINAÇÃO */
    public function updatingBusca(): void
    {
        $this->gotoPage(1);
    }
    public function criar()
    {
        $this->limparCampos();        
    }

    public function abrirModalMaisInformacoes()
    {
        $this->modalMaisInformacoes = true;
    }
    public function fecharModalMaisInformacoes()
    {
        $this->modalMaisInformacoes = false;
    }

    public function abrirModalConfirmarExclusao($id)
    {
        $this->modalConfirmarExclusao = $id;
    }
    public function fecharModalConfirmarExclusao()
    {
        $this->modalConfirmarExclusao = false;
    }

    public function limparCampos()
    {
        $this->busca = "";
        $this->informacoesParoquia = "";
        $this->paroquia_id = 0;
        $this->nome = "";
        $this->paroco = "";
        $this->logomarca = "";
        $this->endereco = "";
        $this->telefone_fixo = "";
        $this->telefone_celular = "";
        $this->whatsapp = "";
        $this->telegram = "";
        $this->signal = "";
        $this->email = "";
        $this->site = "";
    }

    public function editar($id)
    {
        $paroquia = Paroquia::findOrFail($id);
        $this->paroquia_id = $id;
        $this->nome = $paroquia->nome;
        $this->paroco = $paroquia->paroco;
        $this->endereco = $paroquia->endereco;
        $this->telefone_fixo = $paroquia->telefone_fixo;
        $this->telefone_celular = $paroquia->telefone_celular;
        $this->whatsapp = $paroquia->whatsapp;
        $this->telegram = $paroquia->telegram;
        $this->signal = $paroquia->signal;
        $this->email = $paroquia->email;
        $this->site = $paroquia->site;
    }

    public function carregarMaisInformacoes($id)
    {
        $this->informacoesParoquia = Paroquia::findOrFail($id);
        $this->abrirModalMaisInformacoes();
    }

    public function excluir($id)
    {
        $objParoquia = Paroquia::find($id);
        $nomeParoquia = $objParoquia->nome;
        $paroquia = Paroquia::find($id)->delete();
        $log = Log::create([
            "acao" => "Paróquia excluída: " . $nomeParoquia,
            "autor_id" => Auth::user()->id,
        ]);
        session()->flash("message", "PARÓQUIA excluída com sucesso");

        $this->fecharModalConfirmarExclusao();
    }

    public function salvar()
    {
        $regras = [
            'nome' => 'required',
            'paroco' => 'required',
            'logomarca' => 'max:1024',
            'endereco' => 'required',
        ];

        $mensagens = [
            'nome.required' => 'Informe o NOME da PARÓQUIA!',
            'paroco.required' => 'Informe o NOME do PÁROCO!',
            'logomarca.image' => 'Somente arquivos do tipo imagem!',
            'logomarca.max' => 'Somente arquivos de no máximo 1 mega!',
            'endereco.required' => 'Informe o ENDEREÇO da PARÓQUIA!',
        ];

        $this->validate($regras, $mensagens);

        /**NOVO REGISTRO - CRIANDO */
        if ($this->paroquia_id <= 0) {
            $paroquia = Paroquia::create([
                "nome" => $this->nome,
                "paroco" => $this->paroco,
                "usuario_responsavel" => Auth::user()->id,
                "endereco" => $this->endereco,
                "telefone_fixo" => $this->telefone_fixo,
                "telefone_celular" => $this->telefone_celular,
                "whatsapp" => $this->whatsapp,
                "telegram" => $this->telegram,
                "signal" => $this->signal,
                "email" => $this->email,
                "site" => $this->site,
            ]);

            if ($this->logomarca) {                
                if ($caminho = $this->logomarca->store("logomarcas_paroquias","public")) {
                    $paroquia->update([
                        "logomarca" => $caminho,
                    ]);
                }
            }
        }
        /**REGISTRO EXISTENTE - EDITANDO */
        else {
            $paroquia = Paroquia::find($this->paroquia_id);
            $paroquia->update([
                "nome" => $this->nome,
                "paroco" => $this->paroco,
                "endereco" => $this->endereco,
                "telefone_fixo" => $this->telefone_fixo,
                "telefone_celular" => $this->telefone_celular,
                "whatsapp" => $this->whatsapp,
                "telegram" => $this->telegram,
                "signal" => $this->signal,
                "email" => $this->email,
                "site" => $this->site,
            ]);
            if ($this->logomarca) {               
                if ($caminho = $this->logomarca->store("logomarcas_paroquias","public")) {
                    $paroquia->update([
                        "logomarca" => $caminho,
                    ]);
                }
            }
        }

        $log = Log::create([
            "acao" => "Paróquia cadastrada ou editada: " . $this->nome,
            "autor_id" => Auth::user()->id,
        ]);

        session()->flash(
            "message",
            $this->paroquia_id ? "Paróquia ATUALIZADA com sucesso: " . $this->nome : "Paróquia INCLUÍDA com sucesso: " . $this->nome . ""
        );
        $this->limparCampos();
    }
}
