<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Comunidade;
use App\Models\Missa;
use App\Models\Paroquia;
use App\Models\Log;
use App\Models\ParticipanteHasMissa;
use DB;
use Illuminate\Validation\Rules\Exists;

class MissaControlador extends Component
{
    use WithPagination;

    /**VARIÁVEIS E PROPRIEDADES PÚBLICAS */
    public $busca;
    public $paroquia_id = "-1";
    public $informacoesMissa;
    public $missa_id = 0;
    public $data_missa;
    public $hora_missa;
    public $numero_vagas;
    public $vagas_preenchidas = 0;
    public $celebrante;
    public $comunidades_id = "-1";
    public $usuario_responsavel;
    public $ativo = "1";
    public $comunidades;
    public $modalMaisInformacoes = false;
    public $modalConfirmarExclusao = false;
    private $pagination = 20;


    public function render()
    {
        //dd($this->comunidades_id);
        //COMBO PARÓQUIAS
        $this->paroquias = Paroquia::orderBy("nome", "asc")->get();

        if ($this->comunidades_id != "-1" && $this->paroquia_id != "-1") {
            $this->comunidades = DB::table('comunidades')->where('paroquia_id', $this->paroquia_id)->orderBy("nome", "asc")->get();
        }

        if (strlen($this->busca) > 0) {
            $missas = Missa::leftjoin("comunidades as c", "c.id", "missas.comunidades_id")
                ->where("c.nome", "like", "%" . $this->busca . "%")
                ->orWhere("missas.celebrante", "like", "%" . $this->busca . "%")
                ->select("missas.*", "c.nome as comunidade", DB::RAW("'' as vagas_restantes"), DB::RAW("'' as nome_paroquia"))
                ->orderBy("missas.data_missa", "desc")
                ->paginate($this->pagination);

            foreach ($missas as $m) {
                $m->vagas_restantes = $this->calcularVagasRestantes($m->id);
                $m->nome_paroquia = $this->retornarNomeParoquia($m->id);
            }

            return view(
                'livewire.missa',
                [
                    "missas" => $missas,
                    "paroquias" => $this->paroquias,
                    "comunidades" => $this->comunidades,
                ]
            );
        } else {
            $missas = Missa::leftjoin("comunidades as c", "c.id", "missas.comunidades_id")
                ->select("missas.*", "c.nome as comunidade", DB::RAW("'' as vagas_restantes"))
                ->orderBy("missas.data_missa", "desc")
                ->paginate($this->pagination);

            foreach ($missas as $m) {
                $m->vagas_restantes = $this->calcularVagasRestantes($m->id);
                $m->nome_paroquia = $this->retornarNomeParoquia($m->id);
            }

            return view(
                'livewire.missa',
                [
                    "missas" => $missas,
                    "paroquias" => $this->paroquias,
                    "comunidades" => $this->comunidades,
                ]
            );
        }
    }

    public function calcularVagasRestantes($idMissa)
    {
        $missa = Missa::findOrFail($idMissa);
        $numeroVagas = $missa->numero_vagas;
        $vagasPreenchidas = $missa->vagas_preenchidas;
        $vagasRestantes = $numeroVagas - $vagasPreenchidas;
        return $vagasRestantes;
    }

    public function retornarNomeParoquia($idMissa)
    {
        $missa = Missa::findOrFail($idMissa);
        $comunidade = Comunidade::findOrFail($missa->comunidades_id);
        $paroquia = Paroquia::findOrFail($comunidade->paroquia_id);
        return $paroquia->nome;
    }

    /**BUSCAS COM PAGINAÇÃO */
    public function updatingBusca(): void
    {
        $this->gotoPage(1);
    }

    public function limparCampos()
    {
        $this->busca = "";
        $this->missa_id = 0;
        $this->data_missa = "";
        $this->hora_missa = "";
        $this->numero_vagas = "";
        $this->vagas_preenchidas = 0;
        $this->celebrante = "";
    }

    public function editar($id)
    {
        $missa = Missa::findOrFail($id);
        $comunidade = Comunidade::findOrFail($missa->comunidades_id);
        $paroquia = Paroquia::findOrFail($comunidade->paroquia_id);
        $this->missa_id = $id;
        $this->paroquia_id = $paroquia->id;
        $this->data_missa = $missa->data_missa;
        $this->hora_missa = $missa->hora_missa;
        $this->numero_vagas = $missa->numero_vagas;
        $this->vagas_preenchidas = $missa->vagas_preenchidas;
        $this->celebrante = $missa->celebrante;
        $this->comunidades_id = $missa->comunidades_id;
    }

    public function excluir($id)
    {
        //dd($id);
        $objMissa = Missa::find($id);
        $objParticipantesMissa = ParticipanteHasMissa::where("missas_id", $id)
            ->get();
        $quantidadeParticipantes = sizeof($objParticipantesMissa);        

        if ($quantidadeParticipantes <= 0) {
            $missa = Missa::find($id)->delete();
            $log = Log::create([
                "acao" => "Missa excluída",
                "autor_id" => Auth::user()->id,
            ]);
            session()->flash("message", "Missa excluída com sucesso");

            $this->fecharModalConfirmarExclusao();
        } else {
            session()->flash("message", "Não foi possível excluir a MISSA, pois a mesma já possui participantes inscritos!");

            $this->fecharModalConfirmarExclusao();
        }
    }

    public function salvar()
    {
        $regras = [
            'data_missa' => 'required|date|date_format:Y-m-d|after_or_equal:today',           
            'hora_missa' => 'required',
            'numero_vagas' => 'required',
            'numero_vagas' => 'min:1',
            'paroquia_id' => 'not_in:-1',
            'comunidades_id' => 'not_in:-1',
        ];
        $mensagens = [
            'data_missa.required' => 'Informe a DATA da celebração!',
            'data_missa.date_format:Y-m-d' => 'Informe uma DATA válida!',
            'data_missa.after_or_equal' =>'Você não pode informar uma data no PASSADO!', 
            'hora_missa.required' => 'Informe a HORA da celebração',
            'numero_vagas.required' => 'Informe o número de VAGAS para a celebração',
            'numero_vagas.min' => 'A celebração deve ter pelo menos 1 VAGA!',
            'paroquia_id.not_in' => 'Informe a PARÓQUIA onde acontecerá a celebração',
            'comunidades_id.not_in' => 'Informe em qual COMUNIDADE acontecerá a celebração',
        ];

        $this->validate($regras, $mensagens);

        /**NOVO REGISTRO - CRIANDO */
        if ($this->missa_id <= 0) {
            $missa = Missa::create([
                "data_missa" => $this->data_missa,
                "hora_missa" => $this->hora_missa,
                "numero_vagas" => $this->numero_vagas,
                "vagas_preenchidas" => $this->vagas_preenchidas,
                "celebrante" => $this->celebrante,
                "comunidades_id" => $this->comunidades_id,
                "users_id" => Auth::user()->id,
                "ativo" => $this->ativo,
            ]);
        }
        /**REGISTRO EXISTENTE - EDITANDO */
        else {
            $missa = Missa::find($this->missa_id);
            $missa->update([
                "data_missa" => $this->data_missa,
                "hora_missa" => $this->hora_missa,
                "numero_vagas" => $this->numero_vagas,
                "celebrante" => $this->celebrante,
                "comunidades_id" => $this->comunidades_id,
                "users_id" => Auth::user()->id,
                "ativo" => $this->ativo,
            ]);
        }

        $log = Log::create([
            "acao" => "Missa cadastrada ou editada",
            "autor_id" => Auth::user()->id,
        ]);

        session()->flash(
            "message",
            $this->missa_id ? "Missa ATUALIZADA com sucesso" : "Missa INCLUÍDA com sucesso"
        );
        $this->limparCampos();
    }

    public function abrirModalMaisInformacoes()
    {
        $this->modalMaisInformacoes = true;
    }
    public function fecharModalMaisInformacoes()
    {
        $this->modalMaisInformacoes = false;
    }

    public function abrirModalConfirmarExclusao($id)
    {
        $this->modalConfirmarExclusao = $id;
    }
    public function fecharModalConfirmarExclusao()
    {
        $this->modalConfirmarExclusao = false;
    }

    public function carregarMaisInformacoes($id)
    {
        $this->informacoesMissa = Missa::findOrFail($id);
        $this->abrirModalMaisInformacoes();
    }

    /**OUVINTES - ESCUTAR EVENTOS E EXECUTAR AÇÕES */
    protected $listeners = [
        "carregarComunidadesDaParoquiaSelecionada" => "carregarComunidadesDaParoquiaSelecionada",
    ];

    public function carregarComunidadesDaParoquiaSelecionada($idParoquia)
    {
        $this->comunidades = DB::table('comunidades')->where('paroquia_id', $idParoquia)->orderBy("nome", "asc")->get();
    }
}
