<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comunidade extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'paroquia_id',
        'usuario_responsavel',
        'coordenador_1',
        'telefone_coordenador_1',
        'coordenador_2',
        'telefone_coordenador_2',
        'endereco',
        'bairro',
        'cidade',
    ];

    protected $table = "comunidades";

    public function usuario()
    {
        return $this->HasOne(User::class, 'id', 'usuario_responsavel');
    }

    public function paroquia(){
        return $this->HasOne(Paroquia::class, 'id', 'paroquia_id');
    }
}
