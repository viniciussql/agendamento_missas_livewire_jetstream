<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paroquia extends Model
{
    use HasFactory;

            /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'paroco',
        'usuario_responsavel',
        'logomarca',
        'endereco',
        'telefone_fixo',
        'telefone_celular',
        'whatsapp',
        'telegram',
        'signal',
        'email',
        'site',
    ];

    protected $table = "paroquias";

    public function usuario()
    {
        return $this->HasOne(User::class, 'id', 'usuario_responsavel');
    }
}
