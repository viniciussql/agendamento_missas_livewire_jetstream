<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParticipanteHasMissa extends Model
{
    use HasFactory;
    
            /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'missas_id',
        'participantes_id',       
    ];

    protected $table = "participantes_has_missas";
}
