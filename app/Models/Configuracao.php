<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuracao extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_sistema_notificacoes',
        'email_secretario_notificacoes',
        'mensagem_biblica_rodape',
    ];

    protected $table = "configuracoes";
}
