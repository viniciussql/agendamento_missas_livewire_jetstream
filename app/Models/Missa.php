<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Missa extends Model
{
    use HasFactory;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data_missa',
        'hora_missa',
        'numero_vagas',
        'vagas_preenchidas',
        'celebrante',
        'comunidades_id',
        'users_id',
        'ativo',
    ];

    protected $table = "missas";

    public function usuario()
    {
        return $this->HasOne(User::class, 'id', 'usuario_responsavel');
    }

    public function comunidade(){
        return $this->HasOne(Comunidade::class, 'id', 'comunidades_id');
    }

    public function preencherVaga($vagas_preenchidas)
    {
        // Missa Existente
        if ($this->exists)
        {
            $this->attributes['vagas_preenchidas'] =
                $this->attributes['vagas_preenchidas'] + $vagas_preenchidas;
            $this->save();
        }
        return $this;
    }

    public function liberarVaga($vagas_preenchidas){
         // Missa Existente
         if ($this->exists)
         {
             $this->attributes['vagas_preenchidas'] =
                 $this->attributes['vagas_preenchidas'] - $vagas_preenchidas;
             $this->save();
         }
         return $this;
    }
}
