<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParticipanteMissa extends Model
{
    use HasFactory;    
            /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'logradouro',      
        'numero',
        'complemento', 
        'telefone_principal',
        'users_id',  
    ];
    protected $table = "participantes_missa";
}
