<div class="flex h-screen bg-gray-200 items-center justify-center  mt-3 mb-3">
    <div class="grid bg-white rounded-lg shadow-xl w-11/12 md:w-9/12 lg:w-1/2">
        @if (session()->has('message'))
            <div class="bg-teal-100 rounded-b text-teal-900 px-4 py-4 shadow-md my-3" role="alert">
                <div class="flex">
                    <h4>{{ session('message') }}</h4>
                </div>
            </div>
        @endif
        <div class="grid grid-cols-1 mt-5 mx-7">
            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                E-mail para notificações do sistema
            </label>
            @error('email_sistema_notificacoes')<span class="text-red-400">{{ $message }}</span>@enderror
            <input id="email_sistema_notificacoes" wire:model.lazy="email_sistema_notificacoes"
                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                type="email" />
        </div>
        <div class="grid grid-cols-1 mt-5 mx-7">
            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                E-mail do(a) secretário(a) paroquial para notificações
            </label>
            @error('email_secretario_notificacoes')<span class="text-red-400">{{ $message }}</span>@enderror
            <input wire:model.lazy="email_secretario_notificacoes"
                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                type="email" />
        </div>
        <div class="grid grid-cols-1 mt-5 mx-7">
            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                Mensagem bíblica do rodapé
            </label>
            @error('mensagem_biblica_rodape')<span class="text-red-400">{{ $message }}</span>@enderror
            <textarea wire:model.lazy="mensagem_biblica_rodape" cols="30" rows="5"
                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"></textarea>
        </div>
        <div class='flex items-center justify-center  md:gap-8 gap-4 pt-5 pb-5'>
            <button wire:click="salvar()"
                class='w-auto bg-gray-500 hover:bg-gray-700 rounded-lg shadow-xl font-medium text-white px-4 py-2'>Salvar</button>
        </div>
    </div>
</div>
