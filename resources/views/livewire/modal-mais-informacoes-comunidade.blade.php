<!-- component -->
<div class="w-auto h-auto animated fadeIn faster  fixed  left-0 top-0 flex inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover"
    id="modal-id">
    <div class="absolute bg-black opacity-80 inset-0 z-0"></div>
    <div class="bg-white shadow-lg rounded-xl my-auto mx-auto relative p-5 w-auto h-auto">
        <!--content-->
        <div class="">
            <!--body-->
            <div class="p-5 flex-auto">
                <table class="min-w-max w-full table-auto">
                    <thead>
                        <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                            <th colspan="2">
                                <center>
                                    Comunidade {{ $informacoesComunidade->nome }}
                                    <br/>
                                    Paróquia {{ $informacoesComunidade->paroquia->nome }}
                                </center>
                            </th>
                        </tr>                 
                    </thead>
                    <tbody class="text-gray-600 text-sm font-light">
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                Coordenador 1
                            </td>
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                {{ $informacoesComunidade->coordenador_1 }}
                            </td>
                        </tr>
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                Telefone do coordenador 1
                            </td>
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                {{ $informacoesComunidade->telefone_coordenador_1 }}
                            </td>
                        </tr>
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                Coordenador 2
                            </td>
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                {{ $informacoesComunidade->coordenador_2 }}
                            </td>
                        </tr>
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                Telefone do coordenador 2
                            </td>
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                {{ $informacoesComunidade->telefone_coordenador_2 }}
                            </td>
                        </tr>
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                Endereço
                            </td>
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                {{ $informacoesComunidade->endereco }}
                            </td>
                        </tr>
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                Bairro
                            </td>
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                {{ $informacoesComunidade->bairro }}
                            </td>
                        </tr>
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                Cidade
                            </td>
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                {{ $informacoesComunidade->cidade }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <div class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto mb-5">
                    <button wire:click="fecharModalMaisInformacoes()" type="button"
                        class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">Fechar</button>
                </div>
            </div>
            <!--footer-->
        </div>
    </div>
</div>
