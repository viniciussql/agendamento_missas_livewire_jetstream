<div class="overflow-x-auto">
    <div class="min-w-screen h-auto bg-gray-100 flex items-center justify-center bg-gray-100 
    font-sans overflow-hidden">
        <div class="w-full lg:w-5/6">
            <!-- ALERTS - INÍCIO -->
            @if (session()->has('message'))
                <div class="bg-green-800 rounded-b text-white px-4 py-4 shadow-md my-3" role="alert">
                    <div class="flex">
                        <h4>{{ session('message') }}</h4>
                    </div>
                </div>
            @endif
            <!-- ALERTS - FIM -->
            <div id="registros" class="mt-5">
                <a wire:click="limparCampos()" href="#formulario"
                    class="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4">NOVA COMUNIDADE</a>
            </div>
            <!-- BUSCA - INÍCIO -->
            <div class="mt-5 mb-5">
                <input id="txtBusca" wire:model="busca"
                    class="w-full rounded-md bg-gray-200 text-gray-700 leading-tight focus:outline-none py-2 px-2"
                    type="text"
                    placeholder="Pesquise pelo nome da paróquia, nome da comunidade, nome do bairro, nome da cidade, nome do pároco ou pelo endereço">
            </div>
            <!-- BUSCA - FIM -->

            <!-- TABELA REGISTROS - INÍCIO -->
            <div class="bg-white shadow-md rounded my-6">
                <table class="min-w-max w-full table-auto">
                    <thead>
                        <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                            <th class="py-3 px-1 text-left">Comunidade</th>
                            <th class="py-3 px-1 text-left">Bairro</th>
                            <th class="py-3 px-1 text-left">Paróquia</th>
                            <th class="py-3 px-1 text-left">
                                Ações
                            </th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 text-sm font-light">
                        @foreach ($info as $c)
                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ $c->nome }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ $c->bairro }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ $c->paroquia }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            <a href="#formulario" wire:click="editar({{ $c->id }})"
                                                class="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 editar">Editar</a>
                                            <button wire:click="carregarMaisInformacoes({{ $c->id }})"
                                                class="bg-yellow-500 hover:bg-yellow-600 text-white font-bold py-2 px-4">Mais
                                                informações</button>
                                            <button wire:click="abrirModalConfirmarExclusao({{ $c->id }})"
                                                class="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4">Excluir</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="mt-2 mb-2">
                {{ $info->links() }}
            </div>
            <!-- TABELA REGISTROS - FIM -->

            <!-- FORMULÁRIO - INÍCIO -->
            <div id="formulario" class="bg-white shadow-lg rounded-xl my-auto mx-auto relative p-1 w-auto h-auto">
                <div class="">
                    <div class="p-1 flex-auto">
                        <form>
                            <div class="grid bg-white px-4 pt-2 pb-4 sm:p-6 sm:pb-4">
                                <!-- NOME DA COMUNIDADE E PARÓQUIA - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Nome
                                            da
                                            comunidade</label>
                                        @error('nome')<span class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $nome }}" id="nome" wire:model.lazy="nome"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" />
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                                            Paróquia</label>
                                        @error('paroquia_id')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <div wire:ignore>
                                            <select id="paroquia_id" style="width: 100%;"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                wire:model.lazy="paroquia_id">
                                                <option value="-1" selected="selected">Selecione</option>
                                                @foreach ($paroquias as $paroquia)
                                                    <option value="{{ $paroquia->id }}">
                                                        {{ $paroquia->nome }}</option>
                                                @endforeach
                                            </select>
                                            <input style="width: 100%;" wire:model.lazy="endereco_paroquia"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                             value="{{$endereco_paroquia}}" type="text" disabled="disabled">
                                        </div>
                                    </div>
                                </div>
                                <!-- NOME DA COMUNIDADE E PARÓQUIA - FIM -->

                                <!-- COORDENADOR 1 - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Nome
                                            da
                                            coordenador 1</label>
                                        @error('coordenador_1')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $coordenador_1 }}" wire:model.lazy="coordenador_1"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" />
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telefone
                                            do
                                            coordenador 1</label>
                                        <input id="telefone_coordenador_1" value="{{ $telefone_coordenador_1 }}"
                                            wire:model.lazy="telefone_coordenador_1"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" placeholder="(99)999999999" />
                                    </div>
                                </div>
                                <!-- COORDENADOR 1 - FIM -->

                                <!-- COORDENADOR 2 - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Nome
                                            da
                                            coordenador 2</label>
                                        @error('coordenador_2')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $coordenador_2 }}" wire:model.lazy="coordenador_2"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" />
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telefone
                                            do
                                            coordenador 2</label>
                                        <input id="telefone_coordenador_2" value="{{ $telefone_coordenador_2 }}"
                                            wire:model.lazy="telefone_coordenador_2"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" placeholder="(99)999999999" />
                                    </div>
                                </div>
                                <!-- COORDENADOR 2 - FIM -->

                                <!-- ENDEREÇO - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-1 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Endereço</label>
                                        @error('endereco')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $endereco }}"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" wire:model.lazy="endereco">
                                    </div>
                                </div>
                                <!-- ENDEREÇO - FIM -->

                                <!-- BAIRRO E CIDADE - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Bairro</label>@error('bairro')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $bairro }}"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" wire:model.lazy="bairro">
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Cidade</label>@error('cidade')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $cidade }}"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" wire:model.lazy="cidade">
                                    </div>
                                </div>
                                <!-- BAIRRO E CIDADE - FIM -->
                                <!-- BOTÕES - INÍCIO -->
                                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                        <a href="#registros" wire:click.prevent="salvar()" type="button"
                                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">SALVAR</a>
                                    </span>
                                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                        <a href="#registros" wire:click="limparCampos()" type="button"
                                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">VOLTAR
                                            - LISTAGEM</a>
                                    </span>
                                    @if (session()->has('message'))
                                        <span style="color: green"
                                            class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <h4><b>{{ session('message') }}</b></h4>
                                        </span>
                                    @endif
                                </div>
                                <!-- BOTÕES - FIM -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- FORMULÁRIO - FIM -->

            <!-- MODAL MAIS INFORMAÇÕES - INÍCIO -->
            @if ($modalMaisInformacoes)
                @include("livewire.modal-mais-informacoes-comunidade")
            @endif
            <!-- MODAL MAIS INFORMAÇÕES - FIM -->

            <!-- MODAL CONFIRMA EXCLUSÃO - INÍCIO -->
            @if ($modalConfirmarExclusao)
                @include("livewire.modal-confirmacao-exclusao-comunidade")
            @endif
            <!-- MODAL CONFIRMA EXCLUSÃO - FIM -->
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $("#telefone_coordenador_1").mask("(99)999999999");
        $("#telefone_coordenador_2").mask("(99)999999999");
        //$('#paroquia_id').select2();
    });

    document.addEventListener('DOMContentLoaded', function() {
        $("#paroquia_id").on("change", function(e) {
            var id = $("#paroquia_id").val()
            exibirEnderecoParoquia(id)
        });
    });

    $(function() {
        $('a').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1000, 'easeInOutBounce');
            // Outras Animações
            // linear, swing, jswing, easeInQuad, easeInCubic, easeInQuart, easeInQuint, easeInSine, easeInExpo, easeInCirc, easeInElastic, easeInBack, easeInBounce, easeOutQuad, easeOutCubic, easeOutQuart, easeOutQuint, easeOutSine, easeOutExpo, easeOutCirc, easeOutElastic, easeOutBack, easeOutBounce, easeInOutQuad, easeInOutCubic, easeInOutQuart, easeInOutQuint, easeInOutSine, easeInOutExpo, easeInOutCirc, easeInOutElastic, easeInOutBack, easeInOutBounce
        });
        /*$("#nome").focus();*/
    });

    function exibirEnderecoParoquia(id) {        
        window.livewire.emit('exibirEnderecoParoquia', id)
    }
</script>
