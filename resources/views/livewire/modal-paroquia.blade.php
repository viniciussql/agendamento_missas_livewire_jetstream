<!-- component -->
<div class="w-auto h-auto animated fadeIn faster  fixed  left-0 top-0 flex inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover"
    id="modal-id">
    <div class="absolute bg-gray-600 opacity-80 inset-0 z-0"></div>
    <div class="bg-white shadow-lg rounded-xl my-auto mx-auto relative p-1 w-auto h-auto">
        <!--content-->
        <div class="">
            <!--body-->
            <div class="p-1 flex-auto">
                <form>
                    <div class="grid bg-white px-4 pt-2 pb-4 sm:p-6 sm:pb-4">
                        <!-- NOME DA PARÓQUIA E PÁROCO - INÍCIO -->
                        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                            <div class="grid grid-cols-1">
                                <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Nome
                                    da
                                    paróquia</label>
                                @error('nome')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input id="nome" wire:model.lazy="nome"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="text" autofocus />
                            </div>
                            <div class="grid grid-cols-1">
                                <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                                    Pároco</label>
                                @error('paroco')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input wire:model.lazy="paroco"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="text" />
                            </div>
                        </div>
                        <!-- NOME DA PARÓQUIA E PÁROCO - INÍCIO -->
                        <div class="grid grid-cols-1 mt-5 mx-7">
                            <label
                                class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Endereço</label>
                            @error('endereco')<span class="text-red-400">{{ $message }}</span>@enderror
                            <input wire:model.lazy="endereco"
                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                type="text" />
                        </div>
                        <!-- TELEFONE FIXO E TELEFONE CELULAR - INÍCIO -->
                        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                            <div class="grid grid-cols-1">
                                <label
                                    class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telefone
                                    fixo</label>
                                @error('telefone_fixo')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input id="telefone_fixo" wire:model.lazy="telefone_fixo"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="text" />
                            </div>
                            <div class="grid grid-cols-1">
                                <label
                                    class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telefone
                                    celular</label>
                                @error('telefone_celular')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input id="telefone_celular" wire:model.lazy="telefone_celular"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="text" />
                            </div>
                        </div>
                        <!-- TELEFONE FIXO E TELEFONE CELULAR - FIM -->

                        <!-- WHATSAPP E TELEGRAM - INÍCIO -->
                        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                            <div class="grid grid-cols-1">
                                <label
                                    class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Whatsapp</label>
                                @error('whatsapp')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input id="whatsapp" wire:model.lazy="whatsapp"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="text" />
                            </div>
                            <div class="grid grid-cols-1">
                                <label
                                    class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telegram</label>
                                @error('telegram')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input id="telegram" wire:model.lazy="telegram"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="number" min="0" />
                            </div>
                        </div>
                        <!-- WHATSAPP E TELEGRAM - FIM -->

                        <!-- SIGNAL E E-MAIL - INÍCIO -->
                        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                            <div class="grid grid-cols-1">
                                <label
                                    class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Signal</label>
                                @error('signal')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input id="signal" wire:model.lazy="signal"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="text" />
                            </div>
                            <div class="grid grid-cols-1">
                                <label
                                    class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">E-mail</label>
                                @error('email')<span class="text-red-400">{{ $message }}</span>@enderror
                                <input wire:model.lazy="email"
                                    class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                    type="email" />
                            </div>
                        </div>
                        <!-- SIGNAL E E-MAIL - FIM -->

                        <!-- SITE - INÍCIO -->
                        <div class="grid grid-cols-1 mt-5 mx-7 mb-5">
                            <label
                                class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Site</label>
                            @error('site')<span class="text-red-400">{{ $message }}</span>@enderror
                            <input wire:model.lazy="site"
                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                type="text" />
                        </div>
                        <!-- SITE - FIM -->

                        <!-- LOGOMARCA - INÍCIO -->
                        <div class="grid grid-cols-1 mt-5 mx-7">
                            <label
                                class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold mb-1">Logomarca
                                da paróquia</label>
                            <div class='flex items-center justify-center w-full'>
                                <label
                                    class='flex flex-col border-4 border-dashed w-full h-32 hover:bg-gray-100 hover:border-gray-300 group'>
                                    <div class='flex flex-col items-center justify-center pt-7'>
                                        <p
                                            class='lowercase text-sm text-gray-400 group-hover:text-gray-600 pt-1 tracking-wider'>
                                            Selecione uma imagem</p>
                                    </div>
                                    <input wire:model="logomarca" type='file' class="hidden" />
                                </label>
                            </div>
                        </div>
                        <!-- LOGOMARCA - FIM -->

                        <!-- BOTÕES - INÍCIO -->
                        <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                            <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                <button wire:click.prevent="salvar()" type="button"
                                    class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">Salvar</button>
                            </span>
                            <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                <button wire:click="fecharModal()" type="button"
                                    class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">Cancelar</button>
                            </span>
                        </div>
                        <!-- BOTÕES - INÍCIO -->

                    </div>
                </form>
            </div>
            <!--footer-->
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#telefone_fixo").mask("(99) 9999-9999");
        $("#telefone_celular").mask("(99)99999-9999");
        $("#whatsapp").mask("(99)99999-9999");
        $("#telegram").mask("(99)99999-9999");
        $("#signal").mask("(99)99999-9999");
    });
</script>
