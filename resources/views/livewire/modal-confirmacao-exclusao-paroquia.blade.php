<x-jet-confirmation-modal wire:model="modalConfirmarExclusao">
    <x-slot name="title">
        Excluir
    </x-slot>

    <x-slot name="content">
        Tem certeza de que desejar excluir esta <b>PARÓQUIA</b>?
    </x-slot>

    <x-slot name="footer">
        <x-jet-secondary-button wire:click="$set('modalConfirmarExclusao',false)" wire:loading.attr="disabled">
            Cancelar
        </x-jet-secondary-button>

        <x-jet-danger-button class="ml-2" wire:click="excluir({{$modalConfirmarExclusao}})" wire:loading.attr="disabled">
            Deletar
        </x-jet-danger-button>
    </x-slot>
</x-jet-confirmation-modal>