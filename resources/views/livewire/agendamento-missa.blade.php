<div class="overflow-x-auto">
    <div
        class="min-w-screen h-auto bg-gray-100 flex items-center justify-center bg-gray-100 
    font-sans overflow-hidden">
        <div class="w-full lg:w-5/6">
            <!-- ALERTS - INÍCIO -->
            @if (session()->has('message'))
                <div class="bg-green-800 rounded-b text-white px-4 py-4 shadow-md my-3" role="alert">
                    <div class="flex">
                        <h4>{{ session('message') }}</h4>
                    </div>
                </div>
            @endif
            <!-- ALERTS - FIM -->

            <!-- FORMULÁRIO - INÍCIO -->
            <div id="formulario" class="bg-white shadow-lg rounded-xl my-auto mx-auto relative p-1 w-auto h-auto">
                <div class="___class_+?8___">
                    <div class="p-1 flex-auto">
                        <form>
                            <div class="grid bg-white px-4 pt-2 pb-4 sm:p-6 sm:pb-4">
                                <!-- PARÓQUIA - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-1 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Informe
                                            a Paróquia</label>
                                        @error('paroquiaId')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <select id="paroquia" style="width: 100%;"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            wire:model.lazy="paroquiaId">
                                            <option value="-1" selected="selected">Selecione</option>
                                            @foreach ($paroquias as $paroquia)
                                                <option value="{{ $paroquia->id }}">
                                                    {{ $paroquia->nome . ' - ' . $paroquia->endereco }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- PARÓQUIA - FIM -->
                                <!-- COMUNIDADES - INÍCIO -->
                                <div id="dvComunidades"
                                    class="grid grid-cols-1 md:grid-cols-3 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Informe
                                            a Comunidade</label>
                                        @error('comunidadesId')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        @if ($comunidades)
                                            <select wire:model="comunidadesId" style="width: 100%;"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent">
                                                <option value="-1" selected="selected">Selecione</option>
                                                @foreach ($comunidades as $comunidade)
                                                    <option value="{{ $comunidade->id }}">
                                                        {{ $comunidade->nome }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Selecione
                                            o dia e horário da missa desejada</label>
                                        @error('missaId')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        @if ($missas)
                                            <select id="missa_id" style="width: 100%;"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                wire:model="missaId">
                                                <option value="-1" selected="selected">Selecione</option>
                                                @foreach ($missas as $missa)
                                                    <option value="{{ $missa->id }}">
                                                        {{ \Carbon\Carbon::parse($missa->data_missa)->format('d/m/Y') . ' - ' . $missa->hora_missa }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold"
                                            for=""><b> {{ $vagas_restantes_missa }} </b> vagas</label>
                                    </div>
                                </div>
                                <!-- COMUNIDADES - FIM -->
                                <div class="grid grid-cols-1 md:grid-cols-1 gap-5 md:gap-8 mt-5 mx-7">
                                    <div id="dvAdicionaParticipantes" class="grid grid-cols-1">
                                        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <a style="cursor: pointer;"
                                                wire:click.prevent="adicionarParticipante({{ $i }})"
                                                type="button"
                                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">Adicionar
                                                participante(s)</a>
                                        </span>
                                    </div>
                                </div>
                                <!-- ALERTA VAGAS-->
                                <div class="grid grid-cols-1 md:grid-cols-1 gap-5 md:gap-8 mt-3 mx-7">
                                    <div class="grid grid-cols-1">
                                        @if (session()->has('vagas'))
                                            <div class="text-red-800 rounded-b px-4 py-4 shadow-md my-3" role="alert">
                                                <div class="flex">
                                                    <h4>{{ session('vagas') }}</h4>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <!--PARTICIPANTES DA MISSA (CAMPOS DINÂMICOS) - INÍCIO-->
                                @foreach ($participantesMissa as $key => $value)
                                    <div class="grid grid-cols-1 md:grid-cols-6 gap-5 md:gap-8 mt-5 mx-7">
                                        <div class="grid grid-cols-1">
                                            @error('nome_participante_missa.' . $value)<span
                                                class="text-red-400">{{ $message }}</span>@enderror
                                            <input placeholder="Nome completo *"
                                                wire:model.lazy="nome_participante_missa.{{ $value }}"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            @error('logradouro_participante_missa.' . $value)<span
                                                class="text-red-400">{{ $message }}</span>@enderror
                                            <input placeholder="Endereço"
                                                wire:model.lazy="logradouro_participante_missa.{{ $value }}"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            @error('numero_logradouro_participante_missa.' . $value)<span
                                                class="text-red-400">{{ $message }}</span>@enderror
                                            <input placeholder="Número"
                                                wire:model.lazy="numero_logradouro_participante_missa.{{ $value }}"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                type="number" min="1" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            @error('complemento_logradouro_participante_missa.' . $value)<span
                                                class="text-red-400">{{ $message }}</span>@enderror
                                            <input placeholder="Complemento"
                                                wire:model.lazy="complemento_logradouro_participante_missa.{{ $value }}"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            @error('telefone_principal_participante_missa.' . $value)<span
                                                class="text-red-400">{{ $message }}</span>@enderror
                                            <input placeholder="Telefone principal *"
                                                wire:model.lazy="telefone_principal_participante_missa.{{ $value }}"
                                                class="telefone_principal py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                                <a style="cursor: pointer;"
                                                    wire:click.prevent="removerParticipante({{ $key }})"
                                                    type="button"
                                                    class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-800 focus:outline-none focus:border-red-700 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">Remover
                                                    participante</a>
                                            </span>
                                        </div>
                                    </div>
                                    <div style="border:2px solid #c1c1c1; margin-top: 1%;"></div>
                                @endforeach
                                <!--PARTICIPANTES DA MISSA (CAMPOS DINÂMICOS) - FIM-->

                                <!-- BOTÕES - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-4 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1"></div>
                                    <div class="grid grid-cols-1"></div>
                                    <div class="grid grid-cols-1">
                                        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <a style="cursor: pointer;" wire:click="limparCampos()" type="button"
                                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">Cancelar</a>
                                        </span>
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <a style="cursor:pointer;" wire:click.prevent="agendarMissa()" type="button"
                                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">Agendar
                                                Missa</a>
                                        </span>
                                    </div>
                                </div>
                                <!-- BOTÕES - FIM -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- FORMULÁRIO - FIM -->
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".telefone_principal").mask("(99)999999999");
    });
</script>
