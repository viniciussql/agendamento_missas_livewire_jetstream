    <div class="overflow-x-auto">
        <div class="min-w-screen h-auto bg-gray-100 flex items-center justify-center bg-gray-100 font-sans overflow-hidden">
            <div class="w-full lg:w-5/6">
                <!-- ALERTS - INÍCIO -->
                @if (session()->has('message'))
                <div class="bg-green-800 rounded-b text-white px-4 py-4 shadow-md my-3" role="alert">
                    <div class="flex">
                        <h4>{{ session('message') }}</h4>
                    </div>
                </div>
                @endif
                <!-- ALERTS - FIM -->
                <div id="registros" class="mt-5">
                    <a wire:click="limparCampos()" href="#formulario" class="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4">NOVA PARÓQUIA</a>
                </div>

                <!-- BUSCA - INÍCIO -->
                <div class="mt-5 mb-5">
                    <input id="txtBusca" wire:model="busca" class="w-full rounded-md bg-gray-200 text-gray-700 leading-tight focus:outline-none py-2 px-2" type="text" placeholder="Pesquise pelo nome da paróquia, nome do pároco ou endereço">
                </div>
                <!-- BUSCA - FIM -->

                <!-- TABELA REGISTROS - INÍCIO -->
                <div class="bg-white shadow-md rounded my-6">
                    <table class="min-w-max w-full table-auto">
                        <thead>
                            <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th class="py-1 px-1 text-left">Nome da paróquia</th>
                                <th class="py-1 text-left">Endereço</th>
                                <th class="py-1 text-left">
                                    Ações
                                </th>
                            </tr>
                        </thead>
                        <tbody class="text-gray-600 text-sm font-light">
                            @foreach ($info as $p)
                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                <td class="py-1 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ $p->nome }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ $p->endereco }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            <a href="#formulario" wire:click="editar({{ $p->id }})"
                                                 class="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 editar">Editar</a>                                            
                                            <button wire:click="carregarMaisInformacoes({{ $p->id }})" class="bg-yellow-500 hover:bg-yellow-600 text-white font-bold py-2 px-4">Mais
                                                informações</button>                                           
                                            <button wire:click="abrirModalConfirmarExclusao({{ $p->id }})" class="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4">Excluir</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="mt-2 mb-2">
                    {{ $info->links() }}
                </div>
                <!-- TABELA REGISTROS - FIM -->

                <!-- FORMULÁRIO - INÍCIO -->
                <div id="formulario" class="bg-white shadow-lg rounded-xl my-auto mx-auto relative p-1 w-auto h-auto">
                    <!--content-->
                    <div class="">
                        <!--body-->
                        <div class="p-1 flex-auto">
                            <form>
                                <div class="grid bg-white px-4 pt-2 pb-4 sm:p-6 sm:pb-4">
                                    <!-- NOME DA PARÓQUIA E PÁROCO - INÍCIO -->
                                    <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Nome
                                                da
                                                paróquia</label>
                                            @error('nome')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $nome }}" id="nome" wire:model.lazy="nome" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                                                Pároco</label>
                                            @error('paroco')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $paroco }}" wire:model.lazy="paroco" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                    </div>
                                    <!-- NOME DA PARÓQUIA E PÁROCO - FIM -->
                                    <div class="grid grid-cols-1 mt-5 mx-7">
                                        <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Endereço</label>
                                        @error('endereco')<span class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $endereco }}" wire:model.lazy="endereco" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                    </div>
                                    <!-- TELEFONE FIXO E TELEFONE CELULAR - INÍCIO -->
                                    <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telefone
                                                fixo</label>
                                            @error('telefone_fixo')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $telefone_fixo }}" id="telefone_fixo" wire:model.lazy="telefone_fixo" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telefone
                                                celular</label>
                                            @error('telefone_celular')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $telefone_celular }}" id="telefone_celular" wire:model.lazy="telefone_celular" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                    </div>
                                    <!-- TELEFONE FIXO E TELEFONE CELULAR - FIM -->

                                    <!-- WHATSAPP E TELEGRAM - INÍCIO -->
                                    <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Whatsapp</label>
                                            @error('whatsapp')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $whatsapp }}" id="whatsapp" wire:model.lazy="whatsapp" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Telegram</label>
                                            @error('telegram')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $telegram }}" id="telegram" wire:model.lazy="telegram" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                    </div>
                                    <!-- WHATSAPP E TELEGRAM - FIM -->

                                    <!-- SIGNAL E E-MAIL - INÍCIO -->
                                    <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Signal</label>
                                            @error('signal')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $signal }}" id="signal" wire:model.lazy="signal" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">E-mail</label>
                                            @error('email')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $email }}" wire:model.lazy="email" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="email" />
                                        </div>
                                    </div>
                                    <!-- SIGNAL E E-MAIL - FIM -->

                                    <!-- SITE E LOGOMARCA - INÍCIO -->
                                    <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Site</label>
                                            @error('site')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input value="{{ $site }}" wire:model.lazy="site" class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent" type="text" />
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold mb-1">Logomarca
                                                da paróquia</label>
                                            @error('logomarca')<span class="text-red-400">{{ $message }}</span>@enderror
                                            <input wire:model="logomarca" type='file' />
                                        </div>
                                    </div>
                                    <!-- SITE E LOGOMARCAR - FIM -->

                                    <!-- BOTÕES - INÍCIO -->
                                    <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <a href="#registros" wire:click.prevent="salvar()" type="button" class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">SALVAR</a>
                                        </span>
                                        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <a href="#registros" wire:click="limparCampos()" type="button" class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">VOLTAR
                                                - LISTAGEM</a>
                                        </span>
                                        @if (session()->has('message'))
                                        <span style="color: green" class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <h4><b>{{ session('message') }}</b></h4>
                                        </span>
                                        @endif
                                    </div>
                                    <!-- BOTÕES - INÍCIO -->
                                </div>
                            </form>
                        </div>
                        <!--footer-->
                    </div>
                </div>
                <!-- FORMULÁRIO - FIM -->

                <!-- MODAL MAIS INFORMAÇÕES - INÍCIO -->
                @if ($modalMaisInformacoes)
                @include("livewire.modal-mais-informacoes-paroquia")
                @endif
                <!-- MODAL MAIS INFORMAÇÕES - FIM -->

                <!-- MODAL CONFIRMA EXCLUSÃO - INÍCIO -->
                @if ($modalConfirmarExclusao)
                @include("livewire.modal-confirmacao-exclusao-paroquia")
                @endif
                <!-- MODAL CONFIRMA EXCLUSÃO - FIM -->
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#telefone_fixo").mask("(99)9999-9999");
            $("#telefone_celular").mask("(99)99999-9999");
            $("#whatsapp").mask("(99)99999-9999");
            $("#telegram").mask("(99)99999-9999");
            $("#signal").mask("(99)99999-9999");
        });

        $(function() {
            $('a').bind('click', function(event) {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 1000, 'easeInQuad');
                // Outras Animações
                // linear, swing, jswing, easeInQuad, easeInCubic, easeInQuart, easeInQuint, easeInSine, easeInExpo, easeInCirc, easeInElastic, easeInBack, easeInBounce, easeOutQuad, easeOutCubic, easeOutQuart, easeOutQuint, easeOutSine, easeOutExpo, easeOutCirc, easeOutElastic, easeOutBack, easeOutBounce, easeInOutQuad, easeInOutCubic, easeInOutQuart, easeInOutQuint, easeInOutSine, easeInOutExpo, easeInOutCirc, easeInOutElastic, easeInOutBack, easeInOutBounce
            });
            /*$("#nome").focus();*/
        });
    </script>