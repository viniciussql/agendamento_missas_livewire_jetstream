<div class="overflow-x-auto">
    <div
        class="min-w-screen h-auto bg-gray-100 flex items-center justify-center bg-gray-100 
    font-sans overflow-hidden">
        <div class="w-full lg:w-5/6">
            <!-- ALERTS - INÍCIO -->
            @if (session()->has('message'))
                <div class="bg-green-800 rounded-b text-white px-4 py-4 shadow-md my-3" role="alert">
                    <div class="flex">
                        <h4>{{ session('message') }}</h4>
                    </div>
                </div>
            @endif
            <!-- ALERTS - FIM -->
            <div id="registros" class="mt-5">
                <a wire:click="limparCampos()" href="#formulario"
                    class="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4">NOVA MISSA</a>
            </div>
            <!-- BUSCA - INÍCIO -->
            <div class="mt-5 mb-5">
                <input id="txtBusca" wire:model="busca"
                    class="w-full rounded-md bg-gray-200 text-gray-700 leading-tight focus:outline-none py-2 px-2"
                    type="text" placeholder="Pesquise pela comunidade ou pelo celebrante">
            </div>
            <!-- BUSCA - FIM -->

            <!-- TABELA REGISTROS - INÍCIO -->
            <div class="bg-white shadow-md rounded my-6">
                <table class="min-w-max w-full table-auto">
                    <thead>
                        <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                            <th class="py-3 px-1 text-left">Comunidade</th>
                            <th class="py-3 px-1 text-left">Data</th>
                            <th class="py-3 px-1 text-left">Hora</th>
                            <th class="py-3 px-1 text-left">
                                Ações
                            </th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 text-sm font-light">
                        @foreach ($missas as $m)
                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ $m->comunidade }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ Carbon\Carbon::parse($m->data_missa)->format('d/m/Y') }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            {{ $m->hora_missa }}
                                        </div>
                                    </div>
                                </td>
                                <td class="py-3 px-1 text-left whitespace-nowrap">
                                    <div class="flex items-center">
                                        <div class="mr-2">
                                            <a href="#formulario" wire:click="editar({{ $m->id }})"
                                                class="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 editar">Editar</a>
                                            <button wire:click="carregarMaisInformacoes({{ $m->id }})"
                                                class="bg-yellow-500 hover:bg-yellow-600 text-white font-bold py-2 px-4">Mais
                                                informações</button>
                                            <button wire:click="abrirModalConfirmarExclusao({{ $m->id }})"
                                                class="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4">Excluir</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="mt-2 mb-2">
                {{ $missas->links() }}
            </div>
            <!-- TABELA REGISTROS - FIM -->
            <!-- FORMULÁRIO - INÍCIO -->
            <div id="formulario" class="bg-white shadow-lg rounded-xl my-auto mx-auto relative p-1 w-auto h-auto">
                <div class="">
                    <div class=" p-1 flex-auto">
                        <form>
                            <div class="grid bg-white px-4 pt-2 pb-4 sm:p-6 sm:pb-4">
                                <!-- PARÓQUIA - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-1 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Paróquia</label>
                                        @error('paroquia_id')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <select id="paroquia_id" style="width: 100%;"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            wire:model.lazy="paroquia_id">
                                            <option value="-1" selected="selected">Selecione</option>
                                            @foreach ($paroquias as $paroquia)
                                                <option value="{{ $paroquia->id }}">
                                                    {{ $paroquia->nome . ' - ' . $paroquia->endereco }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- PARÓQUIA - FIM -->
                                <!-- COMUNIDADES - INÍCIO -->
                                <div id="dvComunidades"
                                    class="grid grid-cols-1 md:grid-cols-1 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Comunidade</label>
                                        @error('comunidades_id')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        @if ($comunidades)
                                            <select id="comunidades_id" style="width: 100%;"
                                                class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                                wire:model.lazy="comunidades_id">
                                                <option value="-1" selected="selected">Selecione</option>
                                                @foreach ($comunidades as $comunidade)
                                                    <option value="{{ $comunidade->id }}">
                                                        {{ $comunidade->nome }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <!-- COMUNIDADES - FIM -->

                                <!-- DATA E HORA - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                                            Data da celebração
                                        </label>
                                        @error('data_missa')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $data_missa }}" id="data_missa" wire:model.lazy="data_missa"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="date" />
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                                            Hora da celebração
                                        </label>
                                        @error('hora_missa')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $hora_missa }}" id="hora_missa" wire:model.lazy="hora_missa"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="time" />
                                    </div>
                                </div>
                                <!-- DATA E HORA - FIM -->
                                <!-- NÚMERO DE VAGAS E CELEBRANTE - INÍCIO -->
                                <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                                            Número de vagas
                                        </label>
                                        @error('numero_vagas')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $numero_vagas }}" id="numero_vagas"
                                            wire:model.lazy="numero_vagas"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="number" min="1" />
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <label
                                            class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">
                                            Celebrante
                                        </label>
                                        @error('celebrante')<span
                                            class="text-red-400">{{ $message }}</span>@enderror
                                        <input value="{{ $celebrante }}" id="celebrante" wire:model.lazy="celebrante"
                                            class="py-2 px-3 rounded-lg border-2 border-gray-300 mt-1 focus:outline-none focus:ring-2 focus:ring-gray-600 focus:border-transparent"
                                            type="text" />
                                    </div>
                                </div>
                                <!-- NÚMERO DE VAGAS E CELEBRANTE - FIM -->
                                <!-- BOTÕES - INÍCIO -->
                                <div class="px-4 py-4 sm:px-6 sm:flex sm:flex-row-reverse">
                                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                        <a href="#registros" wire:click.prevent="salvar()" type="button"
                                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-800 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">SALVAR</a>
                                    </span>
                                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                        <a href="#registros" wire:click="limparCampos()" type="button"
                                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 text-white">VOLTAR
                                            - LISTAGEM</a>
                                    </span>
                                    @if (session()->has('message'))
                                        <span style="color: green"
                                            class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                            <h4><b>{{ session('message') }}</b></h4>
                                        </span>
                                    @endif
                                    <span style="font-size: 18px; font-style: italic; color:#4682B4;" wire:loading>
                                        Salvando, aguarde...
                                    </span>
                                </div>
                                <!-- BOTÕES - FIM -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- FORMULÁRIO - FIM -->

            <!-- MODAL MAIS INFORMAÇÕES - INÍCIO -->
            @if ($modalMaisInformacoes)
                @include("livewire.modal-mais-informacoes-missa")
            @endif
            <!-- MODAL MAIS INFORMAÇÕES - FIM -->

            <!-- MODAL CONFIRMA EXCLUSÃO - INÍCIO -->
            @if ($modalConfirmarExclusao)
                @include("livewire.modal-confirmacao-exclusao-missa")
            @endif
            <!-- MODAL CONFIRMA EXCLUSÃO - FIM -->
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $("#paroquia_id").on("change", function(e) {
            var idParoquia = $("#paroquia_id").val()
            carregarComunidadesDaParoquiaSelecionada(idParoquia)
        });
    });

    function carregarComunidadesDaParoquiaSelecionada(idParoquia) {
        window.livewire.emit('carregarComunidadesDaParoquiaSelecionada', idParoquia)
    }
</script>
