<?php
use App\Http\Livewire\ConfiguracaoControlador;
use App\Http\Livewire\ParoquiaControlador;
use App\Http\Livewire\ComunidadeControlador;
use App\Http\Livewire\MissaControlador;
use App\Http\Livewire\AgendamentoMissaControlador;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get("/configuracoes", ConfiguracaoControlador::class)->name("configuracoes");
Route::get("/paroquias", ParoquiaControlador::class)->name("paroquias");
Route::get("/comunidades", ComunidadeControlador::class)->name("comunidades");
Route::get("/missas", MissaControlador::class)->name("missas");
Route::get("/agendar-missa", AgendamentoMissaControlador::class)->name("agendar-missa");
